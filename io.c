#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <ctype.h>

#include "io.h"
#include "lexer.h"

void Error(char * s) {
    fprintf(stderr, "Error: %s\n", s);
}

void Abort(char *s, ...) {
    va_list args;
    va_start(args, s);

    char buff[1024];
    vsnprintf(buff, 1024, s, args);
    
    va_end(args);
    
    Error(buff);
    exit(1);
}

void Expected(char *s, char * actual) {
    char buff[1024];
    snprintf(buff, 1023, "Line[%d] Column[%d]: Got '%s', Expected '%s'.", lineNum, charPosition, actual, s);
    Abort(buff);
}

void ExpectedC(char *s, char actual) {
    if(actual == '\n')
    {
        Expected(s, "\\n");
    }
    else {
        char buff[1024];
        snprintf(buff, 1023, "Line[%d] Column[%d]: Got '%c', Expected '%s'.", lineNum, charPosition, actual, s);
        Abort(buff);
    }
}

void ExpectedCC(char c, char actual) {
    if(actual == '\n')
    {
        char buff[2];
        buff[0] = c;
        buff[1] = 0;
        Expected(buff, "\\n");
    }
    else {
        char buff[1024];
        snprintf(buff, 1023, "Line[%d] Column[%d]: Got '%c', Expected '%c'.", lineNum, charPosition, actual, c);
        Abort(buff);
    }
}

void Emit(char *s, ...) {
    va_list args;
    va_start(args, s);
    vfprintf(stdout, s, args);
    va_end(args);
    fflush(stdout);
}

void EmitLn(char *s, ...) {

    va_list args;
    va_start(args, s);
    vfprintf(stdout, s, args);
    fprintf(stdout, "\n");
    va_end(args);
    fflush(stdout);
}

void EmitLnT(char *s, ...) {

    va_list args;
    va_start(args, s);
    fprintf(stdout, "\t");
    vfprintf(stdout, s, args);
    fprintf(stdout, "\n");
    va_end(args);
    fflush(stdout);
}
