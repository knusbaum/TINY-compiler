#include <stdio.h>
#include "io.h"
#include "lexer.h"
#include "parser.h"

int main(void) {
    Init();
    Prog();
    if(Look != -1) fprintf(stderr, "Unexpected input after program: %c\n", Look);
    return 0;
}
