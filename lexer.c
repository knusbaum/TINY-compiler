#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include "lexer.h"
#include "io.h"

char Look;
token_t Token;
char Value[TOKEN_LENGTH];

char * const keywords[] = { "IF", "ELSE", "ENDIF", "WHILE", "ENDWHILE", "VAR", "BEGIN", "END", "PROGRAM", "READ", "WRITE", "WRITESTR", NULL };


int Lookup(char * const table[], char * key) {
    int i = 1;
    while( *table != NULL ) {
        if( StrEq(*table++, key) ) {
            return i;
        }
        i++;
    }
    return 0;
}

int StrEq(char * s1, char * s2) {
    return !strcmp(s1, s2);
}

int charPosition = 0;
void GetChar(void) {
    Look = getchar();
    charPosition++;
}

int lineNum = 1;
void SkipWhite(void) {
    while(Look == ' ' || Look == '\n' || Look == '\t') {
        if(Look == '\n') {
            charPosition = 0;
            lineNum++;
        }
        GetChar();
    }
}

void Init(void) {
    GetChar();
    Scan();
}

void Match(char x) {
    if( Look == x )
        GetChar();
    else {
        char buff[4];
        snprintf(buff, 3, "\'%c\'", x);
        ExpectedCC(x, Look);
    }
    SkipWhite();
}

void MatchString(char * s) {
    if( ! StrEq(s, Value) ) Expected(s, Value);
}

void GetName(void) {

    if(! isalpha(Look)) Expected("Name", "Non-Identifier");
    
    int i;
    for (i = 0; isalnum(Look); i++ ) {
        Value[i] = toupper(Look);
        GetChar();
    }
    Value[i] = 0;

    Token = Identity;

    SkipWhite();
}

void GetNum(void) {

    if (! isdigit(Look)) Expected("Integer", "Non-Digit");
    
    int i;
    for(i = 0; isdigit(Look); i++) {
        Value[i] = Look;
        GetChar();
    }
    Value[i] = 0;
    
    Token = Integer;

    SkipWhite();
}

char * GetString(void) {
    Match('"');
    int strlen = 10;
    char * buff = malloc(strlen * sizeof(char));
    
    int i = 0;
    while(Look != '"') {
        if(i == strlen - 1) {
            strlen *= 2;
            buff = realloc(buff, strlen * sizeof(char));
        } 
        buff[i++] = Look;
        GetChar();
    }
    Match('"');
    return buff;
}

void Scan(void) {
    GetName();
    int i_keyword = Lookup(keywords, Value);
    //if( !i_keyword ) Expected("Keyword", Value);
    
    Token = (token_t)i_keyword;
}
