	## TINY Program ##
	.global _start


	## Global Vars ##
	.data
X:	.int 0
Y:	.int 0
Z:	.int 0


	## Start of Executable ##
	.text
_start:
	leal L0, %eax
	pushl %eax
	call __write_str
	addl $4, %esp
	leal L1, %eax
	pushl %eax
	call __write_str
	addl $4, %esp
	leal L2, %eax
	pushl %eax
	call __write_str
	addl $4, %esp
	leal X, %eax
	pushl %eax
	call __read
	addl $4, %esp
	leal L3, %eax
	pushl %eax
	call __write_str
	addl $4, %esp
	leal Y, %eax
	pushl %eax
	call __read
	addl $4, %esp
	leal L4, %eax
	pushl %eax
	call __write_str
	addl $4, %esp
	leal Z, %eax
	pushl %eax
	call __read
	addl $4, %esp
	movl (X), %eax
	pushl %eax
	movl (Y), %eax
	pushl %eax
	movl (Z), %eax
	movl %eax, %ebx
	popl %eax
	divl %ebx
	popl %ebx
	mull %ebx
	pushl %eax
	call __write
	addl $4, %esp

	## If Construct ##
	movl (X), %eax
	pushl %eax
	movl $10, %eax
	popl %ebx
	cmpl %ebx, %eax
	movl $0, %eax
	setle %al
	test %eax, %eax
	jz L5
	leal L6, %eax
	pushl %eax
	call __write_str
	addl $4, %esp
L5:
	## END If Construct ##


	## While Construct ##
L7:
	movl (Z), %eax
	pushl %eax
	movl $20, %eax
	popl %ebx
	cmpl %ebx, %eax
	movl $0, %eax
	setg %al
	test %eax, %eax
	jz L8
	leal L9, %eax
	pushl %eax
	call __write_str
	addl $4, %esp
	movl (Z), %eax
	pushl %eax
	call __write
	addl $4, %esp
	movl (Z), %eax
	pushl %eax
	movl $1, %eax
	popl %ebx
	addl %ebx, %eax
	movl %eax, (Z)
	jmp L7
L8:
	## END While Construct ##

	leal L10, %eax
	pushl %eax
	call __write_str
	addl $4, %esp
	movl (Z), %eax
	pushl %eax
	call __write
	addl $4, %esp

	## Epilog ##
__end:
	movl %eax, %ebx
	movl $1, %eax
	int $0x80
	## END TINY Program ##

	## IO Routines ##

__read:
	 pushl %ebp
	 movl %esp, %ebp

	 subl $26, %esp
	 movl $0x03, %eax
	 movl $0x00, %ebx
	 movl %esp, %ecx
	 movl $25, %edx
	 int  $0x80

	 cmpl $0, %eax
	 je __io_error

	 movl %eax, %ecx
	 decl %ecx
	 xorl %eax, %eax
	 xorl %ebx, %ebx
	 xorl %esi, %esi

_read_loop:
	 cmpl %esi, %ecx
	 je _read_end

	 movb (%esp,%esi), %bl
	 subl $'0', %ebx

	# Only allow numeric characters
	 cmpl $9, %ebx
	 jg __numeric_error

	 movl $10, %edx
	 mull %edx
	 addl %ebx, %eax
	 incl %esi
	 jmp _read_loop

_read_end:
	 movl 8(%ebp), %ecx
	 movl %eax, (%ecx)
	 movl %ebp, %esp
	 popl %ebp
	 ret

__write:
	 pushl %ebp
	 movl %esp, %ebp

	 movl 8(%ebp), %eax
	 xorl %ecx, %ecx

	 subl $1, %esp
	 movb $'\n', (%esp)
	 subl $1, %esp
	 movb $'\r', (%esp)

_write_loop:
	 test %eax, %eax
	 jz _write_end

	 movl $10, %ebx
	 xorl %edx, %edx
	 divl %ebx
	 addl $'0', %edx
	 subl $1, %esp
	 movb %dl, (%esp)
	 incl %ecx
	 jmp _write_loop
_write_end:

	 addl $2, %ecx
	 movl %ecx, %edx
	 movl $0x04, %eax
	 movl $0x01, %ebx
	 movl %esp, %ecx
	 int $0x80

	 cmpl $-1, %eax
	 je __io_error

	 movl %ebp, %esp
	 popl %ebp
	 ret

__write_str:
	 pushl %ebp
	 movl %esp, %ebp

	 movl 8(%ebp), %edi
	 movl $-1, %ecx
	 movl $-1, %edx
	 movl $0, %eax
	 repne scasb
	 subl %ecx, %edx

	 movl 8(%ebp), %ecx
	 movl $0x04, %eax
	 movl $0x01, %ebx
	 int $0x80

	 cmpl $-1, %eax
	 je __io_error

	 movl %ebp, %esp
	 popl %ebp
	 ret


__io_error:
	 leal _err_io_msg, %eax
	 pushl %eax
	 call __write_str
	 jmp __end


__numeric_error:
	 leal _err_numeric_msg, %eax
	 pushl %eax
	 call __write_str
	 jmp __end

	## Runtime Data ##
	.data
_err_numeric_msg:	.ascii "\nFATAL: You must input integers.\n\0"
_err_io_msg:	.ascii "\nFATAL: IO Error.\n\0"

	## Static Data ##
	.data
L0:	.ascii "Welcome to your first TINY program!\n\0"
L1:	.ascii "Calculating x * y / z\n\0"
L2:	.ascii "X: \0"
L3:	.ascii "Y: \0"
L4:	.ascii "Z: \0"
L6:	.ascii "X is greater than 10.\n\0"
L9:	.ascii "Z is: \0"
L10:	.ascii "Z is: \0"
