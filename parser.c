#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include "parser.h"
#include "io.h"
#include "lexer.h"
#include "codegen.h"

// max 200 (last must be NULL) vars.
#define MAX_VARS 201

char * vars[MAX_VARS] = {NULL};
int lastvar = 0;
int LCount = 0;

char * NewLabel() {
    char * l = malloc(9 * sizeof(char));
    snprintf(l, 9, "L%d", LCount++);
    return l;
}

int InTable(char *s) {
    char **varstab = &vars[0];
    while(*varstab != NULL)
    {
        if( StrEq(*(varstab++), s) ) {
            return 1;
        }
    }
    return 0;
}

void DeclVar(char *s) {
    char * newvar = malloc(TOKEN_LENGTH * sizeof(char));
    strncpy(newvar, s, TOKEN_LENGTH);
    vars[lastvar++] = newvar;
}

//############################ IO Routines ###############################

void Expression(void);

void DoRead(void) {
    Match('(');
    GetName();
    ReadVar();
    while ( Look == ',') {
        Match(',');
        GetName();
        ReadVar();
    }
    Match(')');
}

void DoWrite(void) {
    Match('(');
    Expression();
    WriteVar();
    while( Look == ',') {
        Match(',');
        Expression();
        WriteVar();
    }
    Match(')');
}

void DoWriteStr(void) {
    Match('(');
    char * string = GetString();
    WriteStr(string, NewLabel());
    Match(')');
    
}

//########################### Conditionals  ##############################

void Block(void);
void BoolExpression(void);

void DoIf(void) {
    //Match('i');
    EmitLn("\n\t## If Construct ##");
    BoolExpression();
    char * lab1 = NewLabel();
    char * lab2 = lab1;
    BranchFalse(lab1);
    Block();
    if(Token == ElseSym) {
        lab2 = NewLabel();
        Branch(lab2);
        PostLabel(lab1);
        Block();
    }
    PostLabel(lab2);
    MatchString("ENDIF");

    EmitLnT("## END If Construct ##\n");

    if(lab2 != lab2) {
        free(lab2);
    }
    free(lab1);
}

void DoWhile(void) {
    EmitLn("\n\t## While Construct ##");
    char * lab1 = NewLabel();
    char * lab2 = NewLabel();
    PostLabel(lab1);
    BoolExpression();
    BranchFalse(lab2);
    Block();
    Branch(lab1);
    MatchString("ENDWHILE");
    PostLabel(lab2);
    EmitLnT("## END While Construct ##\n");
}


//########################### Boolean Expressions #########################

int IsOrOp(char c) {
    return (c == '|' || c == '~');
}

int IsAndOp(char c) {
    return (c == '&');
}

int IsRelOp(char c) {
    return ( c == '=' || 
             c == '#' ||
             c == '<' ||
             c == '>');
}

void DoEqual(void) {
    Match('=');
    Expression();
    PopCompare();
    SetEqual();
}

void DoNEqual(void) {
    Match('#');
    Expression();
    PopCompare();
    SetNEqual();
}

void LessOrEqual(void) {
    Match('=');
    Expression();
    PopCompare();
    SetLessOrEqual();
}

void NotEqual(void) {
    Match('>');
    Expression();
    PopCompare();
    SetNEqual();
}

void DoLess(void) {
    Match('<');
    switch(Look) {
    case '=': LessOrEqual(); break;
    case '>': NotEqual(); break;
    default:
        Expression();
        PopCompare();
        SetLess();
        break;
    }
}

void DoGreater(void) {
    Match('>');

    if(Look == '=') {
        Match('=');
        Expression();
        PopCompare();
        SetGreaterOrEqual();
    }
    else {
        Expression();
        PopCompare();
        SetGreater();
    }
}

void Relation(void) {
    Expression();
    while( IsRelOp(Look) ) {
        Push();
        switch(Look) {
        case '=': DoEqual(); break;
        case '#': DoNEqual(); break;
        case '<': DoLess(); break;
        case '>': DoGreater(); break;
        }
    }
}

void NotFactor(void) {
    
    if( Look == '!') {
        Match('!');
        Relation();
        Not();
    }
    else {
        Relation();
    }
}

void DoAnd(void) {
    Match('&');
    NotFactor();
    PopAnd();
}

void BoolTerm(void) {
    NotFactor();
    while( IsAndOp(Look) ) {
        Push();
        DoAnd();
    }
}

void DoXor(void) {
    Match('~');
    BoolTerm();
    PopXor();
}

void DoOr(void) {
    Match('|');
    BoolTerm();
    PopOr();
}

void BoolExpression(void) {
    BoolTerm();
    while( IsOrOp(Look) ) {
        Push();
        switch(Look) {
        case '|': DoOr(); break;
        case '~': DoXor(); break;
        }
    }
}

//########################### Arithmetic Expressions #########################

void Factor(void) {
    if( Look == '(') {
        Match('(');
        BoolExpression();
        Match(')');
    }
    else if ( isalpha(Look) ) {
        GetName();
        LoadVar(Value);
    }
    else {
        GetNum();
        LoadConstant(Value);
    }
}

void FirstFactor(void) {
    if( Look == '+') {
        GetChar();
    }
    else if( Look == '-' ) {
        Factor();
        Negate();
    }
    else {
        Factor();
    }
}

int IsMulOp(char c) {
    return (c == '*' || c == '/');
}

void Term(void);

void Multiply(void) {
    Match('*');
    Term();
    PopMul();
}

void Divide(void) {
    Match('/');
    Term();
    PopDiv();
}

void Rest(void) {
    while( IsMulOp(Look) ) {
        Push();
        switch(Look) {
        case '*': Multiply(); break;
        case '/': Divide(); break;
        }
    }
}

void Term(void) {
    Factor();
    Rest();
}

void FirstTerm(void) {
    FirstFactor();
    Rest();
}

int IsAddOp(char c) {
    return (c == '+' || c == '-');
}

void Add(void) {
    Match('+');
    Term();
    PopAdd();
}

void Subtract(void) {
    Match('-');
    Term();
    PopSub();
}

void Expression(void) {
    FirstTerm();
    while( IsAddOp(Look) ) {
        Push();
        switch(Look) {
        case '+': Add(); break;
        case '-': Subtract(); break;
        }
    }
}

void Assignment(void) {
    char name[TOKEN_LENGTH];
    strncpy(name, Value, TOKEN_LENGTH);
    Match('=');
    BoolExpression();
    Store(name);
}

void Block(void) {
    Scan();
    while (! (Token == EndIfSym || 
              Token == ElseSym ||
              Token == EndWhileSym ||
              Token == EndSym)) {

        switch(Token) {
        case IfSym: DoIf(); break;
        case WhileSym: DoWhile(); break;
        case ReadSym: DoRead(); break;
        case WriteSym: DoWrite(); break;
        case WriteStrSym: DoWriteStr(); break;
        default: Assignment(); break;
        }
        Scan();
    }
}


void Main(void) {
    MatchString("BEGIN");
    Prolog();
    Block();
    MatchString("END");
    Epilog();
}

void Alloc(char *s) {
    if(InTable(s)) Abort("Var %s already declared.", s);
    DeclVar(s);

    Emit("%s:\t.int ", s);
    if(Look == '=') {
        Match('=');
        if( Look == '-') {
            Emit("%c", Look);
            Match('-');
        }
        GetNum();
        EmitLn(Value);
    }
    else {
        EmitLn("0");
    }
}

void Decl(void) {
    GetName();
    Alloc(Value);
    while(Look == ',') {
        Match(',');
        GetName();
        Alloc(Value);
    }
}

void TopDecls(void) {
    EmitLn("");
    EmitLnT("## Global Vars ##");
    EmitLnT(".data");
    
    Scan();
    while( Token != BeginSym ) {
        if( Token == VarSym ) Decl();
        else Expected("Var Declaration", Value);
        Scan();
    }
    EmitLn("");
}

void Prog(void) {
    MatchString("PROGRAM");
    Header();
    TopDecls();
    Main();
    Match('.');
    IORoutines();
    StaticData();
}
