#include <stdlib.h>
#include "codegen.h"
#include "io.h"
#include "parser.h"
#include "lexer.h"

void PostLabelC(char l) {
    EmitLn("%c:", l);
}

void PostLabel(char * l) {
    EmitLn("%s:", l);
}


void Header(void) {
    EmitLnT("## TINY Program ##");
    EmitLnT(".global _start\n");
}

void Prolog(void) {
    EmitLn("");
    EmitLnT("## Start of Executable ##");
    EmitLnT(".text");
    PostLabel("_start");
}

void Epilog(void) {
    EmitLn("");
    EmitLnT("## Epilog ##");
    PostLabel("__end");
    EmitLnT("movl %%eax, %%ebx");
    EmitLnT("movl $1, %%eax");
    EmitLnT("int $0x80");
    EmitLnT("## END TINY Program ##");
}

void Clear(void) {
    EmitLnT("xorl %%eax, %%eax");
}

void Negate(void) {
    EmitLnT("neg %%eax");
}

void LoadConstant(char * constant) {
    EmitLnT("movl $%s, %%eax", constant);
}

void LoadVar(char *name) {
    if(!InTable(name)) Abort("Var %s not declared.", name);
    EmitLnT("movl (%s), %%eax", name);
}

void Push(void) {
    EmitLnT("pushl %%eax");
}

void PopAdd(void) {
    EmitLnT("popl %%ebx");
    EmitLnT("addl %%ebx, %%eax");
}

void PopSub(void) {
    EmitLnT("movl %%eax, %%ebx");
    EmitLnT("popl %%eax");
    EmitLnT("subl %%ebx, %%eax");
}

void PopMul(void) {
    EmitLnT("popl %%ebx");
    EmitLnT("mull %%ebx");
}

void PopDiv(void) {
    EmitLnT("movl %%eax, %%ebx");
    EmitLnT("popl %%eax");
    EmitLnT("divl %%ebx");
}

void Store(char *name) {
    if(!InTable(name)) Abort("Var %s not declared.", name);
    EmitLnT("movl %%eax, (%s)", name);
}

// ##### Boolean #####

void Not(void) {
    EmitLnT("not %%eax");
}

void PopAnd(void) {
    EmitLnT("popl %%ebx");
    EmitLnT("andl %%ebx, %%eax");
}

void PopOr(void) {
    EmitLnT("popl %%ebx");
    EmitLnT("orl %%ebx, %%eax");
}

void PopXor(void) {
    EmitLnT("popl %%ebx");
    EmitLnT("xorl %%ebx, %%eax");
}

void PopCompare(void) {
    EmitLnT("popl %%ebx");
    EmitLnT("cmpl %%ebx, %%eax");
}

void SetEqual(void) {
    EmitLnT("movl $0, %%eax");
    EmitLnT("sete %%al");
}

void SetNEqual(void) {
    EmitLnT("movl $0, %%eax");
    EmitLnT("setne %%al");
}

void SetGreater(void) {
    EmitLnT("movl $0, %%eax");
    EmitLnT("setl %%al");
}

void SetLess(void) {
    EmitLnT("movl $0, %%eax");
    EmitLnT("setg %%al");
}

void SetLessOrEqual(void) {
    EmitLnT("movl $0, %%eax");
    EmitLnT("setge %%al");
}

void SetGreaterOrEqual(void) {
    EmitLnT("movl $0, %%eax");
    EmitLnT("setle %%al");
}

//##### Conditional Constructs #####

void Branch(char * label) {
    EmitLnT("jmp %s", label);
}

void BranchFalse(char * label) {
    EmitLnT("test %%eax, %%eax");
    EmitLnT("jz %s", label);
}

//##### IO Code #####

void ReadVar(void) {
    EmitLnT("leal %s, %%eax", Value);
    EmitLnT("pushl %%eax");
    EmitLnT("call __read");
    EmitLnT("addl $4, %%esp");
}

void WriteVar(void) {
    EmitLnT("pushl %%eax");
    EmitLnT("call __write");
    EmitLnT("addl $4, %%esp");
}

typedef struct {
    char * string;
    char * label;
} data_string;

data_string data_strings[200] = {{NULL, NULL}};
int nextDatStr = 0;


void WriteStr(char *s, char *label) {
    //char * label = NewLabel();
    data_strings[nextDatStr].string = s;
    data_strings[nextDatStr].label = label;
    nextDatStr++;
    
    EmitLnT("leal %s, %%eax", label);
    EmitLnT("pushl %%eax");
    EmitLnT("call __write_str");
    EmitLnT("addl $4, %%esp");
}

void StaticData(void) {
    EmitLn("");
    EmitLnT("## Static Data ##");
    EmitLnT(".data");
    
    int i;
    for(i = 0; i < nextDatStr; i++) {
        EmitLn("%s:\t.ascii \"%s\\0\"", 
               data_strings[i].label,
               data_strings[i].string);
        free(data_strings[i].label);
        free(data_strings[i].string);
    }
}

void IORoutines(void) {
    // Emit the read routine
    EmitLn("");
    EmitLnT("## IO Routines ##");
    EmitLn("\n"
           "__read:"
           "\n\t pushl %%ebp"
           "\n\t movl %%esp, %%ebp"
           "\n"
           "\n\t subl $26, %%esp"
           "\n\t movl $0x03, %%eax"
           "\n\t movl $0x00, %%ebx"
           "\n\t movl %%esp, %%ecx"
           "\n\t movl $25, %%edx"
           "\n\t int  $0x80"
           "\n"
           "\n\t cmpl $0, %%eax"
           "\n\t je __io_error"
           "\n"
           "\n\t movl %%eax, %%ecx"
           "\n\t decl %%ecx"
           "\n\t xorl %%eax, %%eax"
           "\n\t xorl %%ebx, %%ebx"
           "\n\t xorl %%esi, %%esi"
           "\n"
           "\n_read_loop:"
           "\n\t cmpl %%esi, %%ecx"
           "\n\t je _read_end"
           "\n"
           "\n\t movb (%%esp,%%esi), %%bl"
           "\n\t subl $'0', %%ebx"
           "\n"
           "\n\t# Only allow numeric characters"
           "\n\t cmpl $9, %%ebx"
           "\n\t jg __numeric_error"
           "\n"
           "\n\t movl $10, %%edx"
           "\n\t mull %%edx"
           "\n\t addl %%ebx, %%eax"
           "\n\t incl %%esi"
           "\n\t jmp _read_loop"
           "\n"
           "\n_read_end:"
           "\n\t movl 8(%%ebp), %%ecx"
           "\n\t movl %%eax, (%%ecx)"
           "\n\t movl %%ebp, %%esp"
           "\n\t popl %%ebp"
           "\n\t ret"
           "");

    EmitLn("\n"
           "__write:"
           "\n\t pushl %%ebp"
           "\n\t movl %%esp, %%ebp"
           "\n"
           "\n\t movl 8(%%ebp), %%eax"
           "\n\t xorl %%ecx, %%ecx"
           "\n"
           "\n\t subl $1, %%esp"
           "\n\t movb $'\\n', (%%esp)"
           "\n\t subl $1, %%esp"
           "\n\t movb $'\\r', (%%esp)"
           "\n"
           "\n_write_loop:"
           "\n\t test %%eax, %%eax"
           "\n\t jz _write_end"
           "\n"
           "\n\t movl $10, %%ebx"
           "\n\t xorl %%edx, %%edx"
           "\n\t divl %%ebx"
           "\n\t addl $'0', %%edx"
           "\n\t subl $1, %%esp"
           "\n\t movb %%dl, (%%esp)"
           "\n\t incl %%ecx"
           "\n\t jmp _write_loop"
           "\n_write_end:"
           "\n"
           "\n\t addl $2, %%ecx"
           "\n\t movl %%ecx, %%edx"
           "\n\t movl $0x04, %%eax"
           "\n\t movl $0x01, %%ebx"
           "\n\t movl %%esp, %%ecx"
           "\n\t int $0x80"
           "\n"
           "\n\t cmpl $-1, %%eax"
           "\n\t je __io_error"
           "\n"
           "\n\t movl %%ebp, %%esp"
           "\n\t popl %%ebp"
           "\n\t ret"
           "");

    EmitLn("\n"
           "__write_str:"
           "\n\t pushl %%ebp"
           "\n\t movl %%esp, %%ebp"
           "\n"
           "\n\t movl 8(%%ebp), %%edi"
           "\n\t movl $-1, %%ecx"
           "\n\t movl $-1, %%edx"
           "\n\t movl $0, %%eax"
           "\n\t repne scasb"
           "\n\t subl %%ecx, %%edx"
           "\n"
           "\n\t movl 8(%%ebp), %%ecx"
           "\n\t movl $0x04, %%eax"
           "\n\t movl $0x01, %%ebx"
           "\n\t int $0x80"
           "\n"
           "\n\t cmpl $-1, %%eax"
           "\n\t je __io_error"
           "\n"
           "\n\t movl %%ebp, %%esp"
           "\n\t popl %%ebp"
           "\n\t ret"
           "");

    EmitLn("\n"
           "\n__io_error:"
           "\n\t leal _err_io_msg, %%eax"
           "\n\t pushl %%eax"
           "\n\t call __write_str"
           "\n\t jmp __end"
           "");
    
    EmitLn("\n"
           "\n__numeric_error:"
           "\n\t leal _err_numeric_msg, %%eax"
           "\n\t pushl %%eax"
           "\n\t call __write_str"
           "\n\t jmp __end"
           "");
    
    EmitLn("");
    EmitLnT("## Runtime Data ##");
    EmitLnT(".data");
    EmitLn("_err_numeric_msg:\t.ascii \"\\nFATAL: You must input integers.\\n\\0\"");
    EmitLn("_err_io_msg:\t.ascii \"\\nFATAL: IO Error.\\n\\0\"");
           
}
