#ifndef LEXER_H
#define LEXER_H

#define TOKEN_LENGTH 16

void GetChar(void);
void Match(char x);
void MatchString(char *s);
void GetName(void);
void GetNum(void);
char * GetString(void);
void Scan(void);
void Init(void);

int StrEq(char * s1, char * s2);

typedef enum {
    Identity = 0,
    IfSym = 1,
    ElseSym = 2,
    EndIfSym = 3,
    WhileSym = 4,
    EndWhileSym = 5,
    VarSym = 6,
    BeginSym = 7,
    EndSym = 8,
    ProgramSym = 9,
    ReadSym = 10,
    WriteSym = 11,
    WriteStrSym = 12,
    Integer = 13
} token_t;

extern char Look;
extern token_t Token;
extern char Value[TOKEN_LENGTH];
extern int lineNum;
extern int charPosition;

#endif
