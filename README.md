Compilers
=========

Playing with Compilers

## How To
Take a look at the example program.tiny

To build the compiler, run ```make```.


To build the tiny program, run:


    ./a.out < program.tiny > test.s


To assemble the tiny program, run:


    make as


Then run it with:


    ./a.out


That's all there is to it.

# About TINY

Tiny is a language created by Jack Crenshaw in his series *Let's Build a Compiler*

http://www.stack.nl/~marcov/compiler.pdf

### Expressions
Tiny supports arithmetic and boolean logic on its only data type, a 32-bit integer.
Arithmetic and boolean logic work as they do in C; there is no boolean data type, and you may intermix boolean and arithmetic operators. 

### Variables
Tiny only supports global variables declared before the ```BEGIN``` statement. Multiple declarations may be separated by a comma, and initializers are legal. 

These are all fine:

```
var x,y,z
var t=0,u,v=1000
var w
```

### Control Constructs
Tiny only has two control constructs: The If and the While.

See program.tiny for samples.

### I/O
Tiny has three I/O routines: Read(var), Write(var), and WriteStr(string)

Read and Write are variadic. Read will read sequentially into each variable separated by a comma

    read(x,y,z)

will read an integer into x, then an integer into y, then z.

Write will write each argument separated by a comma to stdout on a new line

    write(x,y,z)
    
will give

```
<value of x>
<value of y>
<value of z>
```

WriteStr only takes a single argument, a string in a set of ```"``` characters, and writes it to stdout. You must include your own newline.

This would be a "hello world" program in Tiny:

```
program
begin
WriteStr("Hello, World!")
end.
```

### Targets

Tiny targets 32-bit linux on the x86 architecture, and will run on 32-bit or 64-bit Linux kernels.
